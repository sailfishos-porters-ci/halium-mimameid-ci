#!/bin/sh

# setup build env
sudo zypper -n in lvm2 atruncate pigz
sudo zypper in -y kmod 
sudo zypper rm droid-tools || true # it's ok if you hadn't any
sudo zypper -n in android-tools

#For building super.img
sudo zypper in -y curl
sudo zypper in -y clang
sudo zypper in -y git
sudo zypper in -y zlib-devel
sudo zypper in -y glibc-devel glibc-static libstdc++-devel
sudo zypper in p7zip

echo "Creating loop devices..."
sudo mknod /dev/loop0 -m0660 b 7 0
sudo mknod /dev/loop1 -m0660 b 7 1
sudo mknod /dev/loop2 -m0660 b 7 2
echo $?
ls -lh /dev/loop*

echo "Building Sailfish $RELEASE"

sudo mic create loop --arch=$PORT_ARCH \
--tokenmap=ARCH:$PORT_ARCH,RELEASE:$RELEASE,RNDRELEASE:latest,EXTRA_NAME:$EXTRA_NAME \
--record-pkgs=name,url \
--outdir=sfe-$DEVICE-$RELEASE$EXTRA_NAME \
Jolla-@RELEASE@-$DEVICE-testing-@ARCH@.ks

# create fastboot flashable super.img
find
git clone https://github.com/LonelyFool/lpunpack_and_lpmake.git
cd lpunpack_and_lpmake
export LDFLAGS="-lstdc++fs -L/usr/lib/gcc/aarch64-meego-linux-gnuabi/8.3.0/" 
./make.sh && cd ..
./lpunpack_and_lpmake/bin/lpmake --metadata-size 65536 --metadata-slots 1 --sparse --super-name super --device super:8589934592 --group sailfish:8585740288 --partition system_a:none:8388608000:sailfish --image 'system_a=SailfishOS-mimameid/root.img' --output SailfishOS-mimameid/super.img

rm SailfishOS-mimameid/root.img
7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on SailfishOS-mimameid.7z  SailfishOS-mimameid

